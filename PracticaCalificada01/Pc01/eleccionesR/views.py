from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from .models import Candidato, Region

def index(request):
    ultima_region_lista = Region.objects.order_by('-nombre_region')[:5]
    context = {'ultima_region_lista': ultima_region_lista}
    return render(request, 'index.html', context)

def detalle (request, region_id):
    region = get_object_or_404(Region, pk=region_id)
    return render(request, 'detalle.html', {'region': region})

def votar (request, region_id):
    region = get_object_or_404(Region, pk=region_id)
    try:
        selected_candidato = region.candidato_set.get(pk=request.POST['candidato'])
    except (KeyError, Candidato.DoesNotExist):
        return render(request, 'detalle.html', {
            'region' : region,
            'error_message' : "No has seleccionado nada",
        })
    else:
        selected_candidato.votos += 1
        selected_candidato.save()
        return HttpResponseRedirect(reverse('eleccionesR:resultados', args=(region.id,)))


def resultados(request, region_id):
    region = get_object_or_404(Region, pk=region_id)
    return render(request, 'resultados.html', {'region': region})