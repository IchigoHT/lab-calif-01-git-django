from django.db import models

# Create your models here.
class Region(models.Model):
    nombre_region= models.CharField(max_length=200)

class Candidato(models.Model):
    fk_region = models.ForeignKey(Region, on_delete=models.CASCADE)
    nombre_candidato= models.CharField(max_length=200)
    partido_candidato= models.CharField(max_length=200)
    numero_candidato= models.CharField(max_length=200)
    votos = models.IntegerField(default=0)